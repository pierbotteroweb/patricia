var path = require('path');
const HtmlWebPackPlugin = require("html-webpack-plugin");

module.exports = {
    devtool:'source-map',
    entry: './app/App.js',
    output:{
        path: path.resolve(__dirname, "./js"),
        filename:'bundle.js'
    },
    mode:'production',
    module: {
      rules: [
        {
          test: /\.(js)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        }
      ]
    }
  };
  