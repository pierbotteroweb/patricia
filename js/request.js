$(document).ready(function(){
    $('.form-content').on('submit', function(e){
        e.preventDefault();
        var $this = $(this);
        var url = $this.attr('action');
        var data = $this.serialize(); 

        $.ajax({
            data: data,
            url: url,
            dataType: 'json',
            type: 'POST',
        })

        .done(function(resp){
            if (resp.code == 1)
            {
                swal({
                    type: 'success',
                    title: 'Sucesso',
                    text: 'Mensagem enviada com sucesso.',
                })
            }
            else
            {
                swal({
                    type: 'error',
                    title: 'Erro',
                    text: 'Desculpe, ocorreu um erro tente novamente mais tarde.',
                })
            }
        })

        .fail(function (resp){
            swal({
                type: 'error',
                title: 'Sucesso',
                text: 'Desculpe, ocorreu um erro tente novamente mais tarde.',
            })
        });
    });

    $('.form-newsletter').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        var url = $this.attr('action');
        var data = $this.serialize();

        $('.btn-ok').attr('disabled', 'true');

        $.ajax({
            data: data,
            url: url,
            dataType: 'json',
            type: 'POST',
        })

        .done(function (resp) {
            if (resp.code == 1) {
                swal({
                    type: 'success',
                    title: 'Sucesso',
                    text: 'Newsletter enviada com sucesso.',
                })

                $('.btn-ok').removeAttr('disabled');
            }
            else {
                swal({
                    type: 'error',
                    title: 'Sucesso',
                    text: 'Desculpe, ocorreu um erro tente novamente mais tarde.',
                })

                $('.btn-ok').removeAttr('disabled');
            }
        })

        .fail(function (resp) {
            swal({
                type: 'error',
                title: 'Sucesso',
                text: 'Desculpe, ocorreu um erro tente novamente mais tarde.',
            })
        });
    });
});