import React from 'react';
import ItemDepoimento from './ItemDepoimento';
class DepoimentosMain extends React.Component{
    constructor(){
        super();
        this.state={
            depoimentos:[]
        }
    }


componentDidMount(){
  let self = this;
  fetch('https://servidor-node-mysql.herokuapp.com/depoimentos')
  .then(response=>response.json())
  .then(function(response){
    self.setState({
      depoimentos:response.data
    })
  })
}

render(){
    return(
        this.state.depoimentos.map((item)=>
        <ItemDepoimento depoimentos={item}/>)
    )
}

}

export default DepoimentosMain