var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var webpack = require('webpack');

gulp.task('sass',function(ok){
	return gulp.src('css/*.scss')
	.pipe(sass())
	.pipe(gulp.dest('css'))
	.pipe(browserSync.stream());
	ok()
});

gulp.task('browser',function(){
	browserSync.init({
		server: "./"
	});
	gulp.watch('./**.*')
});

gulp.task('montagem',function(){
    
    webpack(require('./webpack.config.js'),function(err,stats){
      if(err){
        console.log(err.toString());
      }

      console.log("Webpack finalizado")   

    })

	function montaPagina(modulo,final){
		return gulp.src(['src/modules/_header.html','src/modules/'+modulo,'src/modules/_footer.html'])
		.pipe(concat(final))
		.pipe(gulp.dest('./'))
	}

	montaPagina('_index.html','index.html');
	montaPagina('_depoimentos.html','depoimentos.html');
	montaPagina('_sobre.html','sobre.html');
	montaPagina('_contato.html','contato.html');
	montaPagina('_clinica.html','clinica.html');
	montaPagina('_rolfing.html','rolfing.html');
});

gulp.task('default',['montagem','browser','sass'],function(){
	gulp.watch("*.html").on('change',browserSync.reload);
	gulp.watch("css/*.scss",['sass']);
	gulp.watch('src/modules/*.html',['montagem'])
});

